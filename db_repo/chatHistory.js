var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var mongoosePaginate = require('mongoose-paginate');
process.env.TZ = 'Asia/Calcutta';
var chatSchema = new Schema({


    senderId: {
        type: String
    },
    senderName: {
        type: String
    },
    senderImage:{
        type: String
    },
    receiverId: {
        type: String
    },
    receiverProfilePic:{
        type: String
    },
    roomId: {
        type: String
    },

    messageType: {
        type: String
    },
    mediaType: {
        type: String
    },
    media: {
        type: String
    },

    message: {
        type: String
    },

    time: {
        type: Date,
        default: Date.now
    },
    currentTime: {
        type: Date
    },
    status: {
        type: String,
        default: 'SENT'
    },
    hidden: {
        type: [String]
    },
    isEncrypted:{
        type:Boolean,
        default:true
    }
});
chatSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('chatHistory', chatSchema);
